<?php
class Logger{

	function __construct($config){
		$this->PWD = $config['PWD'];
		if (isset($config['LOG_DIR']))
			$this->LOG_DIR  = $config['LOG_DIR'];
		else
			$this->LOG_DIR = "log/";
		if (isset($config['BACKUP_DIR']))
			$this->LOG_DIR  = $config['BACKUP_DIR'];
		else		
			$this->BACKUP_DIR = "backup/";
		if (isset($config['REPORT_DIR']))
                        $this->REPORT_DIR  = $config['REPORT_DIR'];
                else
                        $this->REPORT_DIR = "report/";

		$this->LOG_LEVEL = $config['LOG_LEVEL'];
		$this->SCRIPT = $config['SCRIPT'];
		set_error_handler(array($this, 'handler'));
	}

	function handler($errno, $errstr, $errfile, $errline, $errcontext){
		$this->set_log($errno, $errstr, $errfile, $errline, $errcontext);
	}

	function set_path($path){
		$this->LOG_FILE  = $path;
	}

	function set_level($level){
		$this->LOG_LEVEL = $level;
	}

	function set_access_log ($log, $level=false){
		if ($this->get_log_level($level) < $this->LOG_LEVEL)
			return;

	        $logfile = $this->PWD.$this->LOG_DIR.$this->SCRIPT.'-access-'.date('Ymd').'.log';
                if (file_exists($logfile))
                        $f = fopen($logfile, 'a');
                else{
                        $f = fopen($logfile, 'a');
                        chmod($logfile, 0666);
                }
		list( $msecs, $time ) = explode(' ', microtime());
		$microsecs = substr($msecs, 2, 5);

		if ($level)
                        $level = "[$level]";		

		$log = date('d-m-Y H:i:s', $time).":$microsecs [".getmypid()."] $level $log\n";
                fwrite($f, $log);
                fclose($f);
	}

	function set_backup_log ($log){
		$logfile = $this->PWD.$this->BACKUP_DIR.$this->SCRIPT.'-backup-'.date('YmdH').'.log';
		if (!file_exists($logfile))
			chmod($logfile, 0666);
		
		$log = "$log\n";
		file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
	}

	function set_core_log ($log, $level=false){
                $logfile = $this->PWD.$this->LOG_DIR.$this->SCRIPT."-".date('Ymd').'.log';
		if (file_exists($logfile))
			$f = fopen($logfile, 'a');
		else{
			$f = fopen($logfile, 'a');
			chmod($logfile, 0666);
		}			
		list( $msecs, $time ) = explode( ' ', microtime());
		$microsecs = substr($msecs, 2, 5);

		if ($level)
			$level = "[$level]";

                $log = date('d-m-Y H:i:s', $time).":$microsecs [".getmypid()."] $level $log\n";
                fwrite($f, $log);
                fclose($f);
        }
	function set_report_log ($pcs, $text, $status, $dialog_id, $answer, $application, $shortcode){

		$respuesta = preg_replace("/\s+/", " ", $answer);
		if(in_array($shortcode, array('501','502','503','504','505','506','507','508','509','510','511','512',))) 
			$shortcode = '123';
		
		$text = preg_replace('/[^A-Za-z0-9\-]/', '', $text);
		$log = $pcs."\t".$text."\t".$status."\t".$dialog_id."\t".$application."\t".$respuesta."\t".$shortcode; 
                $logfile = $this->PWD.$this->REPORT_DIR."report-".date('Ymd').'.log';
                if (file_exists($logfile))
                        $f = fopen($logfile, 'a');
                else{
                        $f = fopen($logfile, 'a');
                        chmod($logfile, 0666);
                }
                list( $msecs, $time ) = explode( ' ', microtime());
                $microsecs = substr($msecs, 2, 5);

                $log = date('d-m-Y H:i:s', $time)."\t".$log."\n";
                fwrite($f, $log);
                fclose($f);
        }


	function set_log($errno=false, $errstr=false, $errfile=false, $errline=false, $errcontext=false){
		
		$hostname = $this->get_node_code();
		
		if ($errno >= $this->LOG_LEVEL){
			switch ($errno){
				case E_ERROR: $level = 'ERROR'; break;
				case E_WARNING: $level = 'WARNING'; break;
				case E_PARSE: $level = 'PARSE'; break;
				case E_NOTICE: $level = 'NOTICE'; break;
				case E_CORE_ERROR: $level = 'CORE_ERROR'; break;
				case E_CORE_WARNING: $level = 'CORE_WARNING'; break;
				case E_COMPILE_ERROR: $level = 'COMPILE_ERROR'; break;
				case E_COMPILE_WARNING: $level = 'COMPILE_WARNING'; break;
				case E_USER_ERROR: $level = 'USER_ERROR'; break;
				case E_USER_WARNING: $level = 'USER_WARNING'; break;
				case E_USER_NOTICE: $level = 'USER_NOTICE'; break;
				default: $level = false; break;
			}
			if ($level && (!strpos($errstr, "ORA-24338"))){
				
				$logfile = $this->PWD.$this->LOG_DIR.'error-'.$hostname.'-'.date('Ymd').'.log';
		                if (file_exists($logfile))
		                        $f = fopen($logfile, 'a');
		                else{
		                        $f = fopen($logfile, 'a');
		                        chmod($logfile, 0666);
		                }
				$log = date('d-m-Y H:i:s')." [".getmypid()."] [$level] [$errfile] [$errline] [$errstr]\n";
				fwrite($f, $log);
				fclose($f);
			}
		}
	}

	function get_log_level($level){
		switch ($level){
			case "DEBUG": $level = 0; break;
			case "INFO": $level = 1; break;
			case "WARNING": $level = 2; break;
			case "ERROR": $level = 3; break;
			case "PANIC": $level = 4; break;
			default: $level = 1; break;
		}
		return $level;
	}
	
	public function get_node_code()
    {
        $host = strtolower(gethostname());
        $sindex = strpos($host,'.');
        if($sindex)
            return substr($host, 0,$sindex);
        else
            return $host;
    }
}
?>
